
import junit.framework.TestCase;
import junit.framework.Assert;
import org.junit.Test;
import static org.junit.Assert.*;
public class CaluclatorTest extends TestCase {
    Calculator cal = new Calculator();

    public void testAdd() {
        Assert.assertEquals(cal.add(10, 20), 30);
    }
    public void testMultiply() {
        Assert.assertEquals(cal.mul(10, 20), 200);
    }
}